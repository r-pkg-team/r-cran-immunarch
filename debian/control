Source: r-cran-immunarch
Maintainer: Debian R Packages Maintainers <r-pkg-team@alioth-lists.debian.net>
Uploaders: Steffen Moeller <moeller@debian.org>
Section: gnu-r
Testsuite: autopkgtest-pkg-r
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-r,
               r-base-dev,
               r-cran-ggplot2 (>= 3.1.0),
               r-cran-dplyr (>= 0.8.0),
               r-cran-dtplyr,
               r-cran-data.table (>= 1.12.6),
               r-cran-patchwork,
               r-cran-factoextra,
               r-cran-fpc,
               r-cran-upsetr,
               r-cran-pheatmap (>= 1.0.12),
               r-cran-ggrepel,
               r-cran-reshape2 (>= 1.4.2),
               r-cran-circlize,
               r-cran-mass,
               r-cran-rtsne,
               r-cran-readr,
               r-cran-readxl (>= 1.3.1),
               r-cran-shiny (>= 1.4.0),
               r-cran-shinythemes,
               r-cran-airr,
               r-cran-ggseqlogo,
               r-cran-stringr (>= 1.4.0),
               r-cran-ggalluvial,
               r-cran-rcpp (>= 1.0),
               r-cran-magrittr,
               r-cran-tibble (>= 2.0),
               r-cran-scales,
               r-cran-ggpubr,
               r-cran-rlang (>= 0.4),
               r-cran-plyr,
               r-cran-dbplyr (>= 1.4.0)
Standards-Version: 4.6.0
Vcs-Browser: https://salsa.debian.org/r-pkg-team/r-cran-immunarch
Vcs-Git: https://salsa.debian.org/r-pkg-team/r-cran-immunarch.git
Homepage: https://cran.r-project.org/package=immunarch
Rules-Requires-Root: no

Package: r-cran-immunarch
Architecture: any
Depends: ${R:Depends},
         ${shlibs:Depends},
         ${misc:Depends}
Recommends: ${R:Recommends}
Suggests: ${R:Suggests}
Description: Bioinformatics Analysis of T-Cell and B-Cell Immune Repertoires
 A comprehensive framework for bioinformatics exploratory analysis of
 bulk and single-cell T-cell receptor and antibody repertoires. It
 provides seamless data loading, analysis and visualisation for AIRR
 (Adaptive Immune Receptor Repertoire) data, both bulk immunosequencing
 (RepSeq) and single-cell sequencing (scRNAseq). It implements most of
 the widely used AIRR analysis methods, such as: clonality analysis,
 estimation of repertoire similarities in distribution of clonotypes
 and gene segments, repertoire diversity analysis, annotation of
 clonotypes using external immune receptor databases and clonotype
 tracking in vaccination and cancer studies.  This package is a successor
 to the previously published 'tcR' immunoinformatics package.
